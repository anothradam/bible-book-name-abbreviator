# bible-book-name-abbreviator

A simple library for returning an abbreviated Bible book name.
The input string is not case sensitive and you are able to reverse the behavior of the do funciton to return a full book name from an abbreviation.

## Installation

bible-book-name-abbreviator works in both the browser and [node.js][node].

### Node / NPM

Install via npm:

``` sh
npm install bible-book-name-abbreviator
```

And then `require` or `import` it:

``` javascript
var abbr = require('bible-book-name-abbreviator');
or
import {default as abbr} from 'bible-book-name-abbreviator';
```
## Examples

### Usage

Get a Bible book name abbreviation

``` javascript
var abbr = require('bible-book-name-abbreviator');

abbr.do('Matthew'); // Returns: Mt
abbr.do('Exodus'); // Returns: Ex
abbr.do('1 Kings'); // Returns: 1 Kgs
```

The `do` function can be reversed as well by passing an optional boolean parameter

``` javascript
var abbr = require('bible-book-name-abbreviator');

abbr.do('Mt',true); // Returns: Matthew
abbr.do('ex',true); // Returns: Exodus
abbr.do('1 Kgs',true); // Returns: 1 Kings
```

The `reference` function takes in a reference then returns the full reference with the new abbreviated book name.

``` javascript
abbr.reference('1 Corinthians 6:20-7:12'); // Returns: 1 Cor 6:20-7:12
```
## Map of book names and abbreviations

Genesis : Gen, 
Exodus : Ex, 
Leviticus : Lev, 
Numbers : Num, 
Deuteronomy : Deut, 
Joshua : Jsh, 
Judges : Judg, 
Ruth : Rth, 
1 Samuel : 1 Sam, 
2 Samuel : 2 Sam, 
1 Kings : 1 Kgs, 
2 Kings : 2 Kgs, 
1 Chronicles : 1 Chron, 
2 Chronicles : 2 Chron, 
Ezra : Ezr, 
Nehemiah : Neh, 
Esther : Esth, 
Job : Job, 
Psalm : Pslm, 
Proverbs : Prov, 
Ecclesiastes : Eccles, 
Song of Solomon : Song, 
Isaiah : Isa, 
Jeremiah : Jer, 
Lamentations : Lam, 
Ezekiel : Ezk, 
Daniel : Dan, 
Hosea : Hos, 
Joel : Joel, 
Amos : Amos, 
Obadiah : Obad, 
Jonah : Jon, 
Micah : Mic, 
Nahum : Nah, 
Habakkuk : Hab, 
Zephaniah : Zeph, 
Haggai : Hag, 
Zechariah : Zech, 
Malachi : Mal, 
Matthew : Mt, 
Mark : Mk, 
Luke : Lk, 
John : Jn, 
Acts : Act, 
Romans : Rom, 
1 Corinthians : 1 Cor, 
2 Corinthians : 2 Cor, 
Galatians : Gal, 
Ephesians : Eph, 
Philippians : Phil, 
Colossians : Col, 
1 Thessalonians : 1 Thes, 
2 Thessalonians : 2 Thes, 
1 Timothy : 1 Tim, 
2 Timothy : 2 Tim, 
Titus : Tit, 
Philemon : Phm, 
Hebrews : Heb, 
James : Jas, 
1 Peter : 1 Pet, 
2 Peter : 2 Pet, 
1 John : 1 Jn, 
2 John : 2 Jn, 
3 John : 3 Jn, 
Jude : Jud, 
Revelation : Rev, 

## License

bible-book-name-abbreviator is MIT.

Copyright 2017 Adam Gedney

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
