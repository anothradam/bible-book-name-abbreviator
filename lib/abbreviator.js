'use strict';

var abbreviator = {};

/**
 * Returns an abbreviated Bible book name
 * @returns {string}
 */
abbreviator.do = function (rawBookName, reverse) {
    "use strict";

    var bookName = rawBookName.toLowerCase(),
        abbreviations = [['Genesis', 'Gen'], ['Exodus', 'Ex'], ['Leviticus', 'Lev'], ['Numbers', 'Num'], ['Deuteronomy', 'Deut'], ['Joshua', 'Jsh'], ['Judges', 'Judg'], ['Ruth', 'Rth'], ['1 Samuel', '1 Sam'], ['2 Samuel', '2 Sam'], ['1 Kings', '1 Kgs'], ['2 Kings', '2 Kgs'], ['1 Chronicles', '1 Chron'], ['2 Chronicles', '2 Chron'], ['Ezra', 'Ezr'], ['Nehemiah', 'Neh'], ['Esther', 'Esth'], ['Job', 'Job'], ['Psalm', 'Pslm'], ['Proverbs', 'Prov'], ['Ecclesiastes', 'Eccles'], ['Song of Solomon', 'Song'], ['Isaiah', 'Isa'], ['Jeremiah', 'Jer'], ['Lamentations', 'Lam'], ['Ezekiel', 'Ezk'], ['Daniel', 'Dan'], ['Hosea', 'Hos'], ['Joel', 'Joel'], ['Amos', 'Amos'], ['Obadiah', 'Obad'], ['Jonah', 'Jon'], ['Micah', 'Mic'], ['Nahum', 'Nah'], ['Habakkuk', 'Hab'], ['Zephaniah', 'Zeph'], ['Haggai', 'Hag'], ['Zechariah', 'Zech'], ['Malachi', 'Mal'], ['Matthew', 'Mt'], ['Mark', 'Mk'], ['Luke', 'Lk'], ['John', 'Jn'], ['Acts', 'Act'], ['Romans', 'Rom'], ['1 Corinthians', '1 Cor'], ['2 Corinthians', '2 Cor'], ['Galatians', 'Gal'], ['Ephesians', 'Eph'], ['Philippians', 'Phil'], ['Colossians', 'Col'], ['1 Thessalonians', '1 Thes'], ['2 Thessalonians', '2 Thes'], ['1 Timothy', '1 Tim'], ['2 Timothy', '2 Tim'], ['Titus', 'Tit'], ['Philemon', 'Phm'], ['Hebrews', 'Heb'], ['James', 'Jas'], ['1 Peter', '1 Pet'], ['2 Peter', '2 Pet'], ['1 John', '1 Jn'], ['2 John', '2 Jn'], ['3 John', '3 Jn'], ['Jude', 'Jud'], ['Revelation', 'Rev']];

    for (var i = 0; i < abbreviations.length; i++) {
        var abb = abbreviations[i];

        if (!reverse) {
            if (abb[0].toLowerCase() === bookName) {
                return abb[1];
            }
        } else {
            if (abb[1].toLowerCase() === bookName) {
                return abb[0];
            }
        }
    }

    return rawBookName; //Failsafe return what was passed in
};

/**
 * Takes in a reference and abbreviates the book name
 * @param ref
 * @returns {*}
 */
abbreviator.reference = function (ref) {
    "use strict";

    var ar = ref.split(' '),
        book = ar,
        passages = book.pop(),
        bookName = book.join(' ').trim(),
        abbrBookName = abbreviator.do(bookName);

    return abbrBookName + ' ' + passages;
};

/**
 * Export
 * @type {{}}
 */
module.exports = abbreviator;